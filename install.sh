# install FRONTEND tools

curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
sudo apt-get update && sudo apt-get install yarn
curl -sL https://deb.nodesource.com/setup_9.x -o nodesource_setup.sh
sudo bash nodesource_setup.sh
sudo apt-get install nodejs
sudo npm install -g @angular/cli
sudo npm audit fix

sudo chown -R $USER:$(id -gn $USER) /home/$USER/.config

npm -v

# export WEBDBPATH='/opt/hhm/db/carriers.db3'
# sudo pip install -r requirements.txt
# sudo pip install -e .
# python backend/hhm/cli.py createdb
# python backend/hhm/cli.py createadmin


# systemd
# sudo cp web.service /etc/systemd/system/
# systemctl enable web.service
# systemctl -l status web.service

# systemctl start web.service; systemctl status web.service
# vim /etc/systemd/system/web.service
# sudo systemctl daemon-reload

# cd fe
# ng build prod