import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-status-icon',
  templateUrl: './status-icon.component.html',
  styleUrls: ['./status-icon.component.css']
})
export class StatusIconComponent implements OnInit {

  @Input() status: boolean;

  constructor() { }

  ngOnInit() {
  }

  getIcon() {
    if (this.status === true) {
      return 'check_circle_outline';
    }
    return 'highlight_off';
  }

  getColor() {
    if (this.status === true) {
      return 'lime';
    }
    return '#eea29a';
  }

  getStatus() {
  	if (this.status === true) {
      return 'On';
    }
    return 'Off';
  }
}
