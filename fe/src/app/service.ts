export class Service {
  id: number;
  pid: string;
  name: string;
  state: number;
  cpu: number;
  mem: number;
  info: string;
  date_last_update: string;
}