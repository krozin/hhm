export class Filter {
  sort: string;
  order: string;
  page: number;
}
