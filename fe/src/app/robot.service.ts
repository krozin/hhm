import { Injectable } from '@angular/core';

import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { Service } from './service';

interface GetResponse {
  services: Service[];
  total_count: number;
}

interface StatusResponse {
  status: boolean;
}

interface LogsResponse {
  logs: String[];
  line_index: number;
}

interface IdsResponse {
  ids: Number[];
}

@Injectable({
  providedIn: 'root'
})
export class RobotService {
  getUrl = '/api/services'
  getServiceStatusUrl = '/api/service_status'
  restartServiceUrl = '/api/service_restart'
  getLogsUrl = '/api/get_logs'

  constructor(private http: HttpClient) { }

  getCarriers(sort: string, order: string, page: number,
              perPage: number): Observable<GetResponse> {
    let params = new HttpParams();
    if (sort !== undefined && order !== undefined) {
      params = params.append('sort', sort);
      params = params.append('order', order);
    }
    params = params.append('page', String(page));
    params = params.append('per_page', String(perPage));
    return this.http.get<GetResponse>(this.getUrl, {params})
      .pipe(
        catchError(this.handleError('getCarriers', [])),
        map((response) => {
        	return response as GetResponse;
        }),
      );
  }

  getServiceStatus(): Observable<StatusResponse> {
    return this.http.get<StatusResponse>(this.getServiceStatusUrl)
      .pipe(
        catchError(this.handleError('getServiceStatus', [])),
        map((response) => {
          return response as StatusResponse;
        }),
      );
  }

  getLogs(lineIndex: number): Observable<LogsResponse> {
    let params = new HttpParams();
    params = params.append('line_index', String(lineIndex));
    return this.http.get<LogsResponse>(this.getLogsUrl, {params})
      .pipe(
        catchError(this.handleError('getLogs', [])),
        map((response) => {
          return response as LogsResponse;
        }),
      );
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
