import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { Filter } from './filter';

@Injectable({ providedIn: 'root' })
export class FilterService {
    private localStorageName = 'filters';
    private defaultFilter: Filter = {
        sort: 'cid',
        order: 'asc',
        page: 0,
    };
    private currentFilterSubject: BehaviorSubject<Filter>;
    public currentFilter$: Observable<Filter>;

    constructor() {
        this.currentFilterSubject = new BehaviorSubject<Filter>(
            this.getFromLocalStorage());
        this.currentFilter$ = this.currentFilterSubject.asObservable();
    }

    private getFromLocalStorage(): Filter {
        const filter = JSON.parse(localStorage.getItem(this.localStorageName));
        return filter || this.defaultFilter;
    }

    private setToLocalStorage(filter: Filter) {
        localStorage.setItem(this.localStorageName, JSON.stringify(filter));
    }

    public get currentFilter(): Filter {
        return this.currentFilterSubject.value;
    }

    public get currentFilterQueryParams(): {} {
        const filter = this.currentFilterSubject.value;
        return {
            'sort': filter.sort,
            'order': filter.order,
            'page': filter.page,
        }
    }

    update(sort: string, order: string, page: number) {
        const filter: Filter = {sort, order, page};
        this.setToLocalStorage(filter);
        this.currentFilterSubject.next(filter);
    }
}
