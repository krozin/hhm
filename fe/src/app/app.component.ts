import { Component, OnInit } from '@angular/core';
import { RobotService } from './robot.service';
import { Router } from '@angular/router';
import { of as observableOf } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { FilterService } from './filter.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  serviceStatus$ = this.robotService.getServiceStatus().pipe(
    catchError(err => observableOf({status: false})),
  )

  constructor(private robotService: RobotService,
              public filterService: FilterService,
              public router: Router) { }

  ngOnInit() {}

}
