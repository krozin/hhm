import json
import os
import random
import string
import threading
import time

import click
import signal
import zmq

from hhm.config import CONF


PRINTABLE = set(string.printable)
LOGPATH = os.environ.get('LOGFILE', CONF['logreader']['logfile'])
ADDR = CONF['logreader']['url']


def random_string(n):
    return ''.join(random.choice(
        string.ascii_lowercase + string.digits) for _ in range(n))


def skip_non_ascii(s):
    return filter(lambda x: x in PRINTABLE, s)


class LogReaderService(object):

    SOCKET_POLL_INTERVAL = 500

    def __init__(self):
        self._stopped = False
        self._context = zmq.Context()
        self._socket = self._context.socket(zmq.REP)
        self._poller = zmq.Poller()

        self._reader_thread = threading.Thread(target=self._reader_loop)
        self._logs = []
        self._line_indexes = []

    def stop(self):
        self._stopped = True

    def run(self):
        signal.signal(signal.SIGINT, lambda *args: self.stop())
        signal.signal(signal.SIGTERM, lambda *args: self.stop())

        self._reader_thread.start()
        self._socket.bind(ADDR)
        self._poller.register(self._socket, zmq.POLLIN)

        self._serve()

        self._reader_thread.join()
        self._socket.close()
        self._context.term()

    def _serve(self):
        while not self._stopped:
            socks = dict(self._poller.poll(self.SOCKET_POLL_INTERVAL))
            if socks.get(self._socket) == zmq.POLLIN:
                message = self._socket.recv()
                request = json.loads(message)
                response = self._handle_request(request)
                self._socket.send(json.dumps(response))

    def _handle_request(self, request):
        try:
            index = self._line_indexes.index(request['line_index'])
        except ValueError:
            index = 0
        return {
            'logs': self._logs[index:],
            'line_index': self._line_indexes[-1],
        }

    def _reader_loop(self):
        fname = LOGPATH
        i = 0
        prev_statinfo = os.stat(fname)
        with open(fname, 'r') as f:
            while not self._stopped:
                statinfo = os.stat(fname)
                print("prev_size={}, cur_size={}".format(
                    prev_statinfo.st_size, statinfo.st_size))
                if statinfo.st_size < prev_statinfo.st_size:
                    i = 0
                    f.seek(0)
                prev_statinfo = statinfo

                line = f.readline()
                if line == '':
                    time.sleep(0.5)
                    continue
                line = skip_non_ascii(line)

                self._logs.append(line)
                self._line_indexes.append(i)
                i += 1

                if len(self._logs) > 35:
                    self._logs = self._logs[-35:]
                    self._line_indexes = self._line_indexes[-35:]


@click.command()
@click.option('--debug/-d', default=False)
def main(debug):
    service = LogReaderService()
    try:
        service.run()
    except Exception:
        service.stop()
        raise


if __name__ == '__main__':
    main()
