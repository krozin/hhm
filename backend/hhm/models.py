from sqlalchemy import Column
from sqlalchemy import DateTime
from sqlalchemy import Integer
from sqlalchemy import String
from sqlalchemy import func
from sqlalchemy.ext.declarative import declarative_base

import bcrypt
import itsdangerous

from hhm import db
from hhm.config import CONF

SECRET_KEY = CONF['fe']['secret_key']
TOKEN_EXPIRES = CONF['fe']['token_life_time']

jwt = itsdangerous.TimedJSONWebSignatureSerializer(
    SECRET_KEY, expires_in=TOKEN_EXPIRES)

Base = declarative_base()


class CRUD():

    def save(self):
        session = db.Session()
        if self.id is None:
            session.add(self)
        res = session.commit()
        session.close()
        return res

    def destroy(self):
        session = db.Session()
        session.delete(self)
        res = session.commit()
        session.close()
        return res


class Service(Base, CRUD):
    __tablename__ = 'services'
    id = Column(Integer, primary_key=True)
    pid = Column(Integer)
    name = Column(String(256))
    state = Column(Integer)
    cpu = Column(Integer)
    mem = Column(Integer)
    info = Column(String(1024))
    date_last_update = Column(DateTime, default=func.now())

    def to_dict(self):
        d = {
            'id': self.id,
            'pid': self.pid,
            'name': self.name,
            'state': self.state,
            'cpu': self.cpu,
            'mem': self.mem,
            'info': self.info,
            'date_last_update': self.date_last_update.strftime(
                "%d-%m-%Y %H:%M:%S"),
        }
        return d


class Users(Base, CRUD):
    __tablename__ = 'users'

    id = Column(Integer, primary_key=True)
    username = Column(String(256), unique=True)
    password_hash = Column(String(256))

    @property
    def password(self):
        return None

    @password.setter
    def password(self, password):
        self.password_hash = bcrypt.hashpw(
            password.encode('utf-8'), bcrypt.gensalt(12))

    def verify_password(self, password):
        return bcrypt.checkpw(password.encode(
            'utf-8'), self.password_hash.encode('utf-8'))

    def generate_auth_token(self):
        return jwt.dumps({'id': self.id})

    @staticmethod
    def verify_auth_token(token):
        try:
            data = jwt.loads(token)
        except itsdangerous.SignatureExpired:
            return None  # valid token, but expired
        except itsdangerous.BadSignature:
            return None  # invalid token
        user = Users.get_or_none(id=data['id'])
        return user

    @staticmethod
    def get_or_none(**kargs):
        session = db.Session()

        cid = kargs.get('id')
        username = kargs.get('username')

        if cid:
            exists_flag = session.query(Users.id).filter_by(
                id=cid).scalar() is not None
            if not exists_flag:
                return
            res = session.query(Users).filter_by(id=cid).first()
        elif username:
            exists_flag = session.query(Users.id).filter_by(
                username=username).scalar() is not None
            if not exists_flag:
                return
            res = session.query(Users).filter_by(
                username=username).first()
        session.close()
        return res


def create_models():
    Base.metadata.create_all(db.engine)
