#!/usr/bin/env python
# -*- coding: utf-8 -*-

import datetime
import sys
import time

from multiprocessing import Manager
from multiprocessing import Process
from multiprocessing import Queue

import click

from hhm import pylib
from hhm.config import CONF
from hhm.helpers import get_cpu_mem_cmd_by_pid
from hhm.helpers import get_systemd_pid
from hhm.helpers import is_service_active
from hhm.helpers import load_config
from hhm.helpers import update_db
from hhm.logger import logger

__author__ = 'krozin@gmail.com'


def loop_provider_worker(config, service_queue):
    """ that get services IDs """
    period = config.get('backend').get(
        'getidperiodically', 5)  # every 5 sec by default

    while True:
        try:
            start_date_time = datetime.datetime.now()
            services = config.get('backend').get('services')
            if not services:
                logger.warning("ZERO services were found")
                continue

            services_ids = dict(zip(services, map(get_systemd_pid, services)))

            for k, v in services_ids.items():
                # if not v:
                #     continue
                citem = {k: v}
                try:
                    service_queue.put(citem, timeout=1)
                    logger.info("item={} put to queue. QueueSize={}".format(
                        citem, service_queue.qsize()))
                except Queue.Full:
                    logger.warning(
                        "Queue is full. Size={}".format(service_queue.qsize()))

            logger.info("list of items has been handled, let's sleep")
            end_date_time = datetime.datetime.now()
            delta = (end_date_time - start_date_time).total_seconds()
            logger.info("Handling time delta = {}; {}".format(
                end_date_time - start_date_time, delta))
            time.sleep(period)

        except Exception as e:
            logger.error("Exception. \n {}".format(e))


def loop_spider_worker(config, service_queue, db_queue):
    """ that get IDs from queue and get cpu,mem,state """
    while True:
        try:
            if service_queue.empty():
                time.sleep(2)
                continue
            qitem = service_queue.get()
            name = qitem.items()[0][0]
            pid = qitem.items()[0][1]
            state = is_service_active(qitem.items()[0][0])

            if not pid:
                pid = -1
                cpu = -1
                mem = -1
                info = 'None'
            else:
                cpu, mem, info = get_cpu_mem_cmd_by_pid(pid)

            citem = {
                'name': name,
                'pid': pid,
                'state': state,
                'cpu': cpu,
                'mem': mem,
                'info': info
            }

            logger.info("DB is going to be updated")
            try:
                db_queue.put(citem, timeout=1)
                logger.info("item={} put to DB queue. QueueSize={}".format(
                    citem, db_queue.qsize()))
            except Queue.Full:
                logger.warning("DB Queue is full. Size={}".format(
                    db_queue.qsize()))

        except Exception as e:
            logger.error("Exception. \n{}".format(e))


def loop_db_worker(config, db_queue):
    """ that get item from queue and update db """
    while True:
        try:
            if db_queue.empty():
                time.sleep(2)
                continue
            citem = db_queue.get()
            if citem:
                update_db(citem)
            else:
                continue
        except Exception:
            logger.exception("DB Exception!!!")


def loop_screenshot_worker(filepath, timeout=0.5):
    """ that get screenshot every 0.5 sec """
    while True:
        try:
            pylib.get_screenshot(filepath)
            time.sleep(timeout)
            logger.debug("take a screenshot to {}".format(filepath))
        except KeyboardInterrupt:
            raise
        except Exception as e:
            logger.exception(str(e))


class HHNManager(object):
    """ that is class shall get config and run Provider and Spiders """
    def __init__(self, config):
        self.config = load_config(config)
        self.count_spiders = 2
        self.writer_p = None
        self.reader_ps = []
        self.db_p = None
        self.screenshot_p = None
        self.manager = Manager()

    def run(self):
        services_queue = Queue(100)
        db_queue = Queue(100)
        # manager = Manager()
        # shared_dict = manager.dict()

        self.writer_p = Process(target=loop_provider_worker, args=(
            self.config, services_queue))
        self.writer_p.name = 'HHM_PROVIDER_WORKER'
        self.writer_p.start()

        self.db_p = Process(target=loop_db_worker, args=(
            self.config, db_queue))
        self.db_p.name = 'HHM_DB_WORKER'
        self.db_p.start()

        self.screenshot_p = Process(target=loop_screenshot_worker, args=(
            CONF['backend']['imagepath'], float(
                CONF['backend']['getimagetimeout'])))
        self.screenshot_p.name = 'HHM_SCREENSHOT_WORKER'
        self.screenshot_p.start()

        for i in range(self.count_spiders):
            p = Process(target=loop_spider_worker, args=(
                self.config, services_queue, db_queue))
            p.name = 'HHM_SPIDER_WORKER_{}'.format(i)
            p.start()
            self.reader_ps.append(p)

        self.writer_p.join()
        self.db_p.join()
        self.screenshot_p.join()
        for p in self.reader_ps:
            p.join()

    def stop(self):
        self.writer_p.terminate()
        self.db_p.terminate()
        self.screenshot_p.terminate()
        for p in self.reader_ps:
            p.terminate()


@click.command()
@click.argument('confpath')
def main(confpath):
    try:
        RHHMMANAGER = HHNManager(confpath)
        RHHMMANAGER.run()
    except KeyboardInterrupt:
        RHHMMANAGER.stop()
        sys.exit()


if __name__ == "__main__":
    main()
