import time

import pexpect.fdpexpect
import serial


class SerialConnection(object):

    def __init__(self, port, baudrate, timeout=10):
        self._conn = serial.Serial(port=port, baudrate=baudrate,
                                   timeout=timeout)

    def __enter__(self):
        return self

    @property
    def stream(self):
        return self._conn

    def read(self):
        size = self._conn.inWaiting()
        if size:
            return self._conn.read(size)
        time.sleep(0.01)
        return ''

    def __exit__(self, *args):
        self._conn.close()


class SerialConsole(SerialConnection):

    SHELL_SEPARATOR = '# '

    def __init__(self, port, baudrate, user, password, timeout=10):
        super(SerialConsole, self).__init__(port, baudrate, timeout)
        self.user = user
        self.password = password
        self._fd = pexpect.fdpexpect.fdspawn(self.stream)

    def login(self):
        self._fd.send('\n')
        need_login = bool(self._fd.expect([self.SHELL_SEPARATOR, 'login:']))
        if need_login:
            self._fd.sendline(self.user)
            self._fd.expect('Password:')
            self._fd.sendline(self.password)
            self._fd.expect(self.SHELL_SEPARATOR)

    def exec_cmd(self, cmd):
        self._fd.sendline(cmd)
        self._fd.expect(self.SHELL_SEPARATOR)
        return self._fd.before[len(cmd) + 3:-2]


class seriallibrary(object):

    def __init__(self):
        self._conn = None

    def _check_conn(self):
        if self._conn is None:
            raise Exception('serial console requires login first.')

    def serial_login(self, port, baudrate, user, password, timeout=10):
        self._conn = SerialConsole(
            port, int(baudrate), user, password, float(timeout))
        self._conn.login()

    def serial_exec_cmd(self, cmd):
        self._check_conn()
        return self._conn.exec_cmd(cmd)
