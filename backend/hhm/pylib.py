import errno
import paramiko
import pexpect.fdpexpect
import serial
import shlex
import socket
import subprocess
import telnetlib


__author__ = 'krozin@gmail.com'


def run_cmd(cmd, cwd=None):
    code = None
    out = None
    try:
        pipe = subprocess.Popen(
            ' '.join(shlex.split(cmd)), shell=True, cwd=cwd,
            stderr=subprocess.PIPE, stdout=subprocess.PIPE)
        out, err = pipe.communicate()
        code = pipe.returncode
    except Exception as e:
        return errno.ENOEXEC, str(e)
    return code, out


def run_ssh_cmd(cmd, host='192.168.192.168', port=22,
                user='root', passwd='', timeout=5):
    out = None

    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    try:
        ssh.connect(
            host, port, username=user, password=passwd, timeout=timeout)
    except (socket.timeout, timeout) as e:
        return errno.ETIMEDOUT, str(e)
    except (paramiko.AuthenticationException) as e:
        return errno.ECONNREFUSED, str(e)
    stdin, stdout, stderr = ssh.exec_command(
        ' '.join(shlex.split(cmd)), timeout=timeout)
    err = stderr.readlines()
    out = stdout.readlines()
    if len(err) > 0:
        return errno.ENOEXEC, "".join(err)
    return 0, "".join(out)


def run_telnet_cmd(cmd, host='127.0.0.1', port=23,
                   user='root', passwd='', timeout=5):
    try:
        tn = telnetlib.Telnet(host, port, timeout)
    except socket.timeout as e:
        return errno.ETIMEDOUT, str(e)
    try:
        tn.read_until("login:", timeout=timeout)
        tn.write(user + "\r\n")
        tn.read_until("Password:", timeout=timeout)
        tn.write(passwd + "\r\n")
        tn.write("{}\r\n".format(' '.join(shlex.split(cmd))))
        tn.write("exit\r\n")
        out = tn.read_all()
    except (socket.timeout, timeout) as e:
        return errno.ETIMEDOUT, str(e)
    return 0, out


def run_serial_cmd(
        cmd, port=23, baudrate=9600, user='root', passwd='', timeout=5):
    def login():
        fd.send('\n')
        need_login = bool(fd.expect([SHELL_SEPARATOR, 'login:']))
        if need_login:
            fd.sendline(user)
            fd.expect('Password:')
            fd.sendline(passwd)
            fd.expect(SHELL_SEPARATOR)
        return True

    def exec_cmd():
        fd.sendline(cmd)
        fd.expect(SHELL_SEPARATOR)
        return fd.before[len(cmd) + 3:-2]

    SHELL_SEPARATOR = '# '
    conn = serial.Serial(port=port, baudrate=int(
        baudrate), timeout=float(timeout))
    fd = pexpect.fdpexpect.fdspawn(conn)
    login()
    return exec_cmd()


def get_screenshot(filepath):
    cmd = shlex.split('import -window root {}'.format(str(filepath)))
    r = subprocess.call(cmd)
    if r == 0:
        return True
    return False
