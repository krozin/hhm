import os
import yaml

from hhm import PACKAGE_DIR


def load_config(config):
    if not (os.path.exists(config) and os.path.isfile(config)):
        return None
    with open(config, 'r') as conffile:
        conf = yaml.load(conffile)
    return conf


CONF_PATH = os.path.join(PACKAGE_DIR, '../config.yaml')
CONF = load_config(CONF_PATH)
