import os

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker


from hhm.config import CONF

dbpath = os.environ.get('WEBDBPATH', CONF['db']['path'])
engine = create_engine('sqlite:///{}'.format(dbpath))
Session = sessionmaker()
Session.configure(bind=engine)
